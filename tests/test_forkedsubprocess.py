"""Tests for forkedsubprocess."""

import pytest

from forkedsubprocess import (ForkedSubprocess,
                              ForkedSubprocessNotRunningException)


class TestForkedSubprocess():
    """Test the ForkedSubprocess class."""

    def test_wait_exception_without_running_process(self):
        """Test ForkedSubprocess wait() failure with no running process."""

        process = ForkedSubprocess(['echo', 'hello world'])

        with pytest.raises(ForkedSubprocessNotRunningException):
            process.wait()

    def test_send_exception_without_running_process(self):
        """Test ForkedSubprocess send() failure with no running process."""

        process = ForkedSubprocess(['cat'])

        with pytest.raises(ForkedSubprocessNotRunningException):
            process.send('line')

    def test_stdout(self):
        """Test stdout."""

        process = ForkedSubprocess(['echo', 'hello world'])
        process.run()
        process.wait()

        assert process.output == ['hello world'], 'Our test string "hello world" should be returned in the .output property'
        assert process.stdout == ['hello world'], 'Our test string "hello world" should be returned in the .stdout property'
        assert process.stderr == [], 'Nothing should be returned in the .stderr property'

    def test_stderr(self):
        """Test stderr."""

        process = ForkedSubprocess(['sh', '-c', 'echo "test stderr" >&2'])
        process.run()
        process.wait()

        assert process.output == ['test stderr'], 'Our test string "test stderr" should be returned in the .output property'
        assert process.stdout == [], 'Nothing should of been returned in the .stdout property'
        assert process.stderr == ['test stderr'], 'Our test string "test stderr" should of been returned in the .stderr property'

    def test_stdout_stderr(self):
        """Test stdout and stderr."""

        process = ForkedSubprocess(['sh', '-c', 'echo "test stdout"; echo "test stderr" >&2'])
        process.run()
        process.wait()

        assert len(process.output) == 2, 'We should have two lines returned'
        assert 'test stdout' in process.output, 'Our test strings "test stdout" should be returned in the .output property'
        assert 'test stderr' in process.output, 'Our test strings "test stderr" should be returned in the .output property'
        assert process.stdout == ['test stdout'], 'Our test string "test stdout" should of been returned in the .stdout property'
        assert process.stderr == ['test stderr'], 'Our test string "test stderr" should of been returned in the .stderr property'

    def test_console_output(self):
        """Test stdout and stderr."""

        process = ForkedSubprocess(['sh', '-c', 'echo "test stdout"; echo "test stderr" >&2'], enable_output=True)
        process.run()
        process.wait()

        assert len(process.output) == 2, 'We should have two lines returned'
        assert 'test stdout' in process.output, 'Our test strings "test stdout" should be returned in the .output property'
        assert 'test stderr' in process.output, 'Our test strings "test stderr" should be returned in the .output property'
        assert process.stdout == ['test stdout'], 'Our test string "test stdout" should of been returned in the .stdout property'
        assert process.stderr == ['test stderr'], 'Our test string "test stderr" should of been returned in the .stderr property'

    def test_callback_output(self):
        """Test callback output."""

        output_lines = []

        def output_callback(line: str):
            """Output callback."""
            output_lines.append(line)

        process = ForkedSubprocess(['sh', '-c', 'echo "test stdout"; echo "test stderr" >&2'], output_callback=output_callback)
        process.run()
        process.wait()

        assert process.output == ['test stdout', 'test stderr'], \
            'Our test strings ["test stdout", "test stderr"] should be returned in the .output property'
        assert process.stdout == ['test stdout'], 'Our test string "test stdout" should of been returned in the .stdout property'
        assert process.stderr == ['test stderr'], 'Our test string "test stderr" should of been returned in the .stderr property'
        assert process.stderr == ['test stderr'], 'Our test string "test stderr" should of been returned in the .stderr property'
        assert len(output_lines) == 2, 'The output_callback should of gotten two lnes'
        assert 'test stdout' in output_lines, 'Our test string "test stdout" should of been sent via callback'
        assert 'test stderr' in output_lines, 'Our test string "test stderr" should of been sent via callback'

    def test_failed_process(self):
        """Test failed process."""

        process = ForkedSubprocess(['false'])
        process.run()
        returncode = process.wait()

        assert process.output == [], 'Nothing should of been output'
        assert returncode == 1, 'The returncode we got should be 1'

    def test_piped_stdout(self):
        """Test piped stdout."""

        process = ForkedSubprocess(['cat'])
        process.run()
        process.send('p1')
        process.send('p2')
        process.send('p3')
        process.wait()

        assert process.output == ['p1', 'p2', 'p3'], 'Our test string "piped output" should be returned in the .output property'
        assert process.stdout == ['p1', 'p2', 'p3'], 'Our test string "hello world" should be returned in the .stdout property'
        assert process.stderr == [], 'Nothing should be returned in the .stderr property'
